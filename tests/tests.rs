use roman_numerals::modern_a_romà;

#[test]
fn nombres_base() {
    assert_eq!(modern_a_romà(5), Ok("V".to_string()));
    assert_eq!(modern_a_romà(1), Ok("I".to_string()));
    assert_eq!(modern_a_romà(100), Ok("C".to_string()));
}

#[test]
fn compostos_sumant() {
    assert_eq!(modern_a_romà(6), Ok("VI".to_string()));
    assert_eq!(modern_a_romà(7), Ok("VII".to_string()));
    assert_eq!(modern_a_romà(8), Ok("VIII".to_string()));
    assert_eq!(modern_a_romà(103), Ok("CIII".to_string()));
    assert_eq!(modern_a_romà(105), Ok("CV".to_string()));
    assert_eq!(modern_a_romà(1005), Ok("MV".to_string()));
    assert_eq!(modern_a_romà(55), Ok("LV".to_string()));
}

#[test]
fn compostos_restant() {
    assert_eq!(modern_a_romà(900), Ok("CM".to_owned()));
    assert_eq!(modern_a_romà(9), Ok("IX".to_owned()));
    assert_eq!(modern_a_romà(4), Ok("IV".to_owned()));
    assert_eq!(modern_a_romà(19), Ok("XIX".to_owned()));
}
