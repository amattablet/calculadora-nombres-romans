pub fn modern_a_romà(mut n: usize) -> Result<String, String> {
    let diccionari_romà: Vec<(usize, String)> = vec![
        (1000, "M".to_owned()),
        (900, "CM".to_owned()),
        (500, "D".to_owned()),
        (400, "CD".to_owned()),
        (100, "C".to_owned()),
        (90, "XC".to_owned()),
        (50, "L".to_owned()),
        (40, "XL".to_owned()),
        (10, "X".to_owned()),
        (9, "IX".to_owned()),
        (5, "V".to_owned()),
        (4, "IV".to_owned()),
        (1, "I".to_owned()),
    ];

    if n > 3999 {
        return Err("Nombre major de 3999, no es pot".to_owned());
    }
    let mut sortida = String::new();

    for (k, c) in diccionari_romà {
        let mut cnt = 0;
        while n >= k && cnt < 3 {
            sortida.push_str(&c);
            n -= k;
            cnt += 1;
        }
    }

    Ok(sortida)
}
