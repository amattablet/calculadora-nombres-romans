#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // Que no s'obri la consola al
                                                                   // Windows quan és compilat en release
                                                                   // mode

use roman_numerals::modern_a_romà;

use eframe::egui;

fn main() {
    // Logs a stdout (si s'executa amb `RUST_LOG=debug`).
    tracing_subscriber::fmt::init();

    let options = eframe::NativeOptions::default();
    eframe::run_native(
        "My egui App",
        options,
        Box::new(|_cc| Box::new(MyApp::default())),
    );
}

struct MyApp {
    arabic: String,
    romà: String,
    comptador: usize,
    ha_passat_tick: bool,
    cheat_sheet_activada: bool,
    nombres_dits: Vec<String>,
}

impl MyApp {
    fn correcte(&self) -> Result<bool, String> {
        let entrada_numerica = {
            match self.arabic.parse::<usize>() {
                Ok(n) => n,
                Err(_) => return Err("Número invàlid".to_string()),
            }
        };
        let romà_real = match modern_a_romà(entrada_numerica) {
            Ok(s) => s,
            Err(e) => return Err(e),
        };

        Ok(romà_real == self.romà)
    }
    fn generar_nombre(&mut self) {
        use rand::prelude::*;
        let mut rng = rand::thread_rng();
        loop {
            let r: usize = rng.gen_range(0..4000); // generates a float between 0 and 1
            if !self.nombres_dits.contains(&r.to_string()) {
                self.arabic = r.to_string();
                break;
            }
        }
    }
}
impl Default for MyApp {
    fn default() -> Self {
        Self {
            arabic: "3".to_string(),
            romà: "VI".to_string(),
            comptador: 0,
            ha_passat_tick: true,
            cheat_sheet_activada: false,
            nombres_dits: Vec::new(),
        }
    }
}

impl eframe::App for MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.heading("Posa't a prova amb els números romans!");
            if ui.button("Genera un número aleatòriament!").clicked() {
                self.generar_nombre();
            }

            let cheat_sheet = format!(
                "\n{}\n{}\n{}\n{}\n{}\n{}\n{}",
                "M: 1000", "D:  500", "C:  100", "L:  50", "X:  10", "V:  5", "I:   1"
            );

            ui.horizontal(|ui| {
                ui.label("Introdueix el número romà: ");
                ui.text_edit_singleline(&mut self.romà);
            });

            let és_correcte: String = match self.correcte() {
                Ok(b) => {
                    if b {
                        if !self.ha_passat_tick && !self.nombres_dits.contains(&self.arabic) {
                            self.nombres_dits.push(self.arabic.clone());
                            self.comptador += 1;
                            self.cheat_sheet_activada = false;
                            self.ha_passat_tick = true;
                        }
                        "Correcte! Molt ben fet :)".to_string()
                    } else {
                        self.ha_passat_tick = false;
                        format!("Incorrecte; Torna-ho a intentar!")
                    }
                }
                Err(e) => e,
            };

            ui.label(format!( "\nHas de traduir el número: {}", self.arabic));
            ui.label(format!(
                "\nDius que {} = {}. Això és {}",
                self.arabic, self.romà, és_correcte
            ));
            ui.label(format!("\nEn portes {} de correctes.", self.comptador));

            if ui.button("Ensenya/Amaga la taula de valors").clicked() {
                self.cheat_sheet_activada = !self.cheat_sheet_activada;
            }

            if self.cheat_sheet_activada {
                ui.label(cheat_sheet);
            }
        });
    }
}
